package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;

public class TestMediaCompareTo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Media> media = new java.util.ArrayList();
		List<String> listAuthors = new ArrayList<String>();
		
		// Create DVD
		DigitalVideoDisc dvd1 = new DigitalVideoDisc(1, "eha", "animation", "john", 100, 32.5f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc(3, "bha", "asdfa", "jack", 100, 33.5f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc(5, "ada", "movie", "edla", 100, 31.5f);
		
		// create Book
		listAuthors.add("Jame");
		listAuthors.add("Elly");
		Book book1 = new Book(2, "aab", "science fiction", 34.5f, listAuthors);
		
		// create CD
		Track track1 = new Track("hello", 2343);
		Track track2 = new Track("hi", 13);
		Track track3 = new Track ("Bye", 32);
		
		CompactDisc cd1 = new CompactDisc(6, "aba", "dfsg", "sdfgs", 4.7f);
		cd1.addTrack(track1); 
		cd1.addTrack(track2);
		
		CompactDisc cd2 = new CompactDisc(7, "nana", "podf", "adfad", 5.7f);
		cd2.addTrack(track2); 
		cd2.addTrack(track3);
		
		// Add the DVD , CD, Book objects to the ArrayList
		media.add(dvd2);
		media.add(dvd1);
		media.add(dvd3);
		media.add(book1);
//		media.add(cd1);
//		media.add(cd2);
		
		// Iterate through the ArrayList and output their titles (unsorted order)
		java.util.Iterator iterator = media.iterator();
		
		System.out.println("---------------------------------------");
		System.out.println("The Media title currently in the order are: ");
		
		while(iterator.hasNext()) {
			System.out.println(((Media)iterator.next()).getTitle());
		}
		
		// Sort the media of DVDs - bases on the compareTo() method
		java.util.Collections.sort(media);
		
		// Iterate through the ArrayList and out put their titles - in sorted order
		iterator = media.iterator();
		System.out.println("------------------------------------------------");
		System.out.println("The Media title in sorted order are : ");
		
		while(iterator.hasNext()) {
			System.out.println(((Media)iterator.next()).getTitle());
		}
		
		System.out.println("------------------------------------------------");
	}

}
