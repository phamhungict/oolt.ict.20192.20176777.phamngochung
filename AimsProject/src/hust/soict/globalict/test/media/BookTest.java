package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;

public class BookTest {
	public static void main(String[] args) {	
		Scanner input = new Scanner(System.in);
		
		System.out.print("ID: ");
		int idBook = input.nextInt();
		input.nextLine();
		System.out.print("TITLE: ");
		String titleBook = input.nextLine();
		System.out.print("CATEGORY: ");
		String categoryBook = input.nextLine();
		System.out.print("AUTHORS (separate by ',' character) : ");
		String authorsBookList = input.nextLine();
		String [] authorsList = authorsBookList.split(",");
		List<String> authorsBook = new ArrayList<String>();
		for(int i = 0; i < authorsList.length; i++) {
			authorsBook.add(authorsList[i]);
		}
		System.out.print("COST: ");
		float costBook = input.nextFloat();
		input.nextLine();
		System.out.print("CONTENT: ");
		String contentBook = input.nextLine();
		
		Book newBook = new Book(idBook, titleBook, categoryBook,costBook, authorsBook, contentBook);
		
		newBook.processContent(contentBook);
		
		System.out.println(newBook.toString());
		
	}
}
