package hust.soict.globalict.aims.GUI;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Warning extends JFrame {
	public Warning(String warning) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JDialog jDialog = new JDialog(this, "Warning");
		JLabel jLabel = new JLabel(warning, SwingConstants.CENTER);
		jDialog.add(jLabel);
		jDialog.setSize(300, 100);
		jDialog.setVisible(true);
	}
}
