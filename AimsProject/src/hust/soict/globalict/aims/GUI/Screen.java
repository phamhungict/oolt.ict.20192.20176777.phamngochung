package hust.soict.globalict.aims.GUI;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import hust.soict.globalict.aims.PlayerException;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;

public class Screen extends JFrame{
	private JPanel panelLanguage;
	private JPanel subPane = new JPanel();
	private JPanel trackPanel = new JPanel(); 
	private JTable table = new JTable();
	private JTable table1 = new JTable();
	private JTable table2 = new JTable();
	private JPanel contentPane;
	private String commandString;
	private String clickString;
	private Order order;
	private String titleEnter, categoryEnter, directorEnter, artistEnter, titleTrackEnter;
	private int idEnter, lengthEnter, deleteEnter, lengthTrackEnter;
	private float costEnter;
	private List<String> authorEnter = new ArrayList<String>();
	private List<Track> tracks = new ArrayList<Track>();
	private JTextField idField = new JTextField();
	private JTextField titleField = new JTextField();
	private JTextField categoryField = new JTextField();
	private JTextField costField = new JTextField();

	
	public String getTitleEnter() {
		return titleEnter;
	}

	public void setTitleEnter(String titleEnter) {
		this.titleEnter = titleEnter;
	}
	
	public String getCategoryEnter() {
		return categoryEnter;
	}
	
	public void setCategoryEnter(String categoryEnter) {
		this.categoryEnter = categoryEnter;
	}

	public int getIdEnter() {
		return idEnter;
	}
	
	public void setIdEnter(int idEnter) {
		this.idEnter = idEnter;
	}

	public float getCostEnter() {
		return costEnter;
	}

	public void setCostEnter(float costEnter) {
		this.costEnter = costEnter;
	}
	
	public String getClickString() {
		return clickString;
	}

	public void setClickString(String clickString) {
		this.clickString = clickString;
	}
	
	public String getCommandString() {
		return commandString;
	}
	
	public void setCommandString(String commandString) {
		this.commandString = commandString;
	}

	public Screen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		subPane = new JPanel();
		subPane.setBorder(new EmptyBorder(5,5,5,5));
		subPane.setPreferredSize(new Dimension(600, 500));
		contentPane.add(subPane);
		this.add(contentPane);
		this.setTitle("Aims Project");
		this.pack();
		setBounds(600, 600, 600, 600);
		this.setJMenuBar(Menu());
	}
	
	public void showGUIOption(String command) {
		switch(command) {
			case "Create New Order": 
				this.getContentPane().removeAll();
				contentPane.removeAll();
				order = new Order();
				JLabel labelOrder = new JLabel();
				labelOrder.setText("Create Order Successfully");
				contentPane.add(labelOrder);
				this.add(contentPane);
				this.revalidate();
				this.repaint();
				break;
			case "Add Item To Order": 
				this.getContentPane().removeAll();
				contentPane.removeAll();
				this.add(contentPane);
				if (order == null){
					JLabel labelOrderNull = new JLabel();
					labelOrderNull.setText("You have to CREATE ORDER FIRST");
					contentPane.add(labelOrderNull);
				}
				else {
					JPanel title = new JPanel();
					JLabel labelOrderMedia = new JLabel();
					labelOrderMedia.setText("Choose the MEDIA you wanna add to order: ");
					labelOrderMedia.setPreferredSize(new Dimension(360, 25));
					title.add(labelOrderMedia);
					contentPane.add(title);
//					JPanel subMenu = new JPanel();
					JComboBox mediaBox = new JComboBox();
					mediaBox.setModel(new DefaultComboBoxModel(new String[] {"CD", "Book", "DVD"}));
					mediaBox.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							String mediaString = mediaBox.getSelectedItem().toString();
							setClickString(mediaString);
							showMediaOption(getClickString());
						}
					});
					contentPane.add(mediaBox);
				}
				this.revalidate();
				this.repaint();
				break;
			case "Delete Item By Id": 
				this.getContentPane().removeAll();
				contentPane.removeAll();
				this.add(contentPane);
				if (order == null){
					JLabel labelOrderNull = new JLabel();
					labelOrderNull.setText("You have to CREATE ORDER FIRST");
					contentPane.add(labelOrderNull);
				}
				else {
					JPanel deleteMedia = new JPanel();
					JLabel delete = new JLabel();
					delete.setText("Enter the ID you wanna delete: ");
					delete.setPreferredSize(new Dimension(400, 25));
					deleteMedia.add(delete);
					contentPane.add(deleteMedia);
					JPanel deleteJPanel = new JPanel();
					JTextField deleteField = new JTextField();
					deleteField.setPreferredSize(new Dimension(400, 25));
					deleteField.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							String info = deleteField.getText();	
							deleteEnter = Integer.parseInt(info);
						}
					});
					deleteJPanel.add(deleteField);
					JButton deleteButton = new JButton("REMOVE");
					deleteButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							order.removeMediaById(deleteEnter);
						}
					});
					deleteJPanel.add(deleteButton);
					contentPane.add(deleteJPanel);
				}
				this.revalidate();
				this.repaint();
				order.removeMediaById(deleteEnter);
				break;
			case "Display Item List Of Order":
				this.getContentPane().removeAll();
				contentPane.removeAll();
				if (order == null){
					JLabel labelOrderNull = new JLabel();
					labelOrderNull.setText("You have to CREATE ORDER FIRST");
					contentPane.add(labelOrderNull);
				}
				else {
					JScrollPane scrollPaneTable = new JScrollPane();
					scrollPaneTable.setBounds(10, 11, 414, 329);
					contentPane.add(scrollPaneTable);
					scrollPaneTable.setViewportView(table);
					
					DefaultTableModel model = new DefaultTableModel();
					model.addColumn("ID");
					model.addColumn("TITLE");
					model.addColumn("CATEGORY");
					model.addColumn("COST");
					
					table.setModel(model);
		
					for(Media item: order.getItemOrdered()) {
						model.addRow(new Object[] {item.getId(), item.getTitle(), item.getCategory(), item.getCost()});
					}
					
					table.setModel(model);
				}
				
				JPanel buttonShow = new JPanel();
				buttonShow.setPreferredSize(new Dimension(200, 25));
				JButton buttonShowDetail = new JButton("SHOW DETAIL");
				buttonShowDetail.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						int index;
						index = table.getSelectedRow();
						order.getItemOrdered().get(index).showDetail();
					}
				});
				
				buttonShow.add(buttonShowDetail);
				contentPane.add(buttonShow);
				
				JPanel totalJPanel = new JPanel();
				
				JLabel totalJLabel = new JLabel();
				totalJLabel.setPreferredSize(new Dimension(200, 25));
				totalJLabel.setText("TOTAL COST: ");
				totalJPanel.add(totalJLabel);
				
				JTextField totalField = new JTextField();
				totalField.setPreferredSize(new Dimension(250, 25));
				String totalString = String.valueOf(order.totalLuckyCost(order.getAluckyItem()));
				totalField.setText(totalString);
				totalJPanel.add(totalField);
				contentPane.add(totalJPanel);
				
				this.add(contentPane);
				this.revalidate();
				this.repaint();
				break;
			case "Lucky Item":
				this.getContentPane().removeAll();
				contentPane.removeAll();
				if (order == null){
					JLabel labelOrderNull = new JLabel();
					labelOrderNull.setText("You have to CREATE ORDER FIRST");
					contentPane.add(labelOrderNull);
				}
				else {
					int k = 0;
					for(int i = 0; i < order.getItemOrdered().size(); i++) {
						if(order.getAluckyItem() == i) {
							k = 1;
							JLabel luckyItemJLabel = new JLabel();
							luckyItemJLabel.setText("ID: " + String.valueOf(order.getItemOrdered().get(i).getId())+ " TITLE : "+ order.getItemOrdered().get(i).getTitle()
									+ " CATEGOTY: " + order.getItemOrdered().get(i).getCategory() + " COST: " + String.valueOf(order.getItemOrdered().get(i).getCost()));
							contentPane.add(luckyItemJLabel);
						}
					}
					if(k == 0) {
						JLabel noLuckyItemJLabel = new JLabel();
						noLuckyItemJLabel.setText("NO LUCKY ITEM");
						contentPane.add(noLuckyItemJLabel);
					}
				}
				this.add(contentPane);
				this.revalidate();
				this.repaint();
				break;
			case "Exit": 
				this.getContentPane().removeAll();
				contentPane.removeAll();
				JLabel exitLabel = new JLabel();
				exitLabel.setText("EXIT !!!");
				contentPane.add(exitLabel);
				this.add(contentPane);
				this.revalidate();
				this.repaint();
				break;
		}
	}

	private void showInfo() {
		// ID
		JPanel idJPanel = new JPanel();
		idJPanel.setPreferredSize(new Dimension(600, 50));
		
		JLabel id = new JLabel();
		id.setText("ID: ");
		id.setPreferredSize(new Dimension(200, 25));
		idJPanel.add(id);
		
		idField.setPreferredSize(new Dimension(300, 25));
		idJPanel.add(idField);
		subPane.add(idJPanel);
		
		// TITLE
		JPanel titleJPanel = new JPanel();
		titleJPanel.setPreferredSize(new Dimension(600, 50));
		
		JLabel title = new JLabel();
		title.setText("TITLE: ");
		title.setPreferredSize(new Dimension(200, 25));
		titleJPanel.add(title);

		titleField.setPreferredSize(new Dimension(300, 25));
		titleJPanel.add(titleField);
		subPane.add(titleJPanel);
		
		// CATEGORY
		JPanel categoryJPanel = new JPanel();
		categoryJPanel.setPreferredSize(new Dimension(600, 50));
		
		JLabel category = new JLabel();
		category.setText("CATEGORY: ");
		category.setPreferredSize(new Dimension(200, 25));
		categoryJPanel.add(category);
		
		categoryField.setPreferredSize(new Dimension(300, 25));
		categoryJPanel.add(categoryField);
		subPane.add(categoryJPanel);
		
		// COST
		JPanel costJPanel = new JPanel();
		costJPanel.setPreferredSize(new Dimension(600, 50));
		
		JLabel cost = new JLabel();
		cost.setText("COST: ");
		cost.setPreferredSize(new Dimension(200, 25));
		costJPanel.add(cost);
		
		costField = new JTextField();
		costField.setPreferredSize(new Dimension(300, 25));
		costJPanel.add(costField);
		subPane.add(costJPanel);
	}
	
	protected void showMediaOption(String click) {
		switch(click) {
			case "CD":
				subPane.removeAll();
				showInfo();
				
				// ARTIST
				JPanel artistJPanel = new JPanel();
				
				JLabel artist = new JLabel();
				artist.setText("ARTIST: ");
				artist.setPreferredSize(new Dimension(200, 25));
				artistJPanel.add(artist);
				
				JTextField artistField = new JTextField();
				artistField.setPreferredSize(new Dimension(300, 25));
				artistJPanel.add(artistField);
				subPane.add(artistJPanel);
				
				// TRACK
				JButton addTrackButton = new JButton("ADD TRACK");
				// TABLE
				JPanel tableTrackPanel1 = new JPanel();
				tableTrackPanel1.setPreferredSize(new Dimension(200, 120));
				JScrollPane scrollPaneTable = new JScrollPane();
				scrollPaneTable.setPreferredSize(new Dimension(200, 115));
				tableTrackPanel1.add(scrollPaneTable);
				subPane.add(tableTrackPanel1);
				scrollPaneTable.setViewportView(table1);
				DefaultTableModel model1 = new DefaultTableModel();
				model1.addColumn("TITLE");
				model1.addColumn("LENGTH");
				
				table1.setModel(model1);
				//event addTrack
				addTrackButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JFrame addTrackFrame = new JFrame();
						addTrackFrame.setSize(400,400);
						addTrackFrame.setTitle("ADD TRACK");
						addTrackFrame.setVisible(true);
						// 
						JPanel container = new JPanel();
						JPanel titleTrackJPanel = new JPanel();
						
						JLabel titleTrackJLabel = new JLabel();
						titleTrackJLabel.setText("TITLE: ");
						titleTrackJLabel.setPreferredSize(new Dimension(100, 25));
						titleTrackJPanel.add(titleTrackJLabel);
						
						JTextField titleTrackField = new JTextField();
						titleTrackField.setPreferredSize(new Dimension(200, 25));
						titleTrackField.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								titleTrackEnter = titleTrackField.getText();
							}
						});
						titleTrackJPanel.add(titleTrackField);
						container.add(titleTrackJPanel);
						
						JPanel lengthTrackJPanel = new JPanel();
						
						JLabel lengthTrackJLabel = new JLabel();
						lengthTrackJLabel.setText("LENGTH: ");
						lengthTrackJLabel.setPreferredSize(new Dimension(100, 25));
						lengthTrackJPanel.add(lengthTrackJLabel);
						
						JTextField lengthTrackField = new JTextField();
						lengthTrackField.setPreferredSize(new Dimension(200, 25));
						lengthTrackField.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {	
								String info = titleTrackField.getText();
								lengthTrackEnter = Integer.parseInt(info);
							}
						});
						lengthTrackJPanel.add(lengthTrackField);
						container.add(lengthTrackJPanel);
						
						
						JButton addTrackButton = new JButton("ADD TRACK");
						addTrackButton.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								Track track = new Track(titleTrackField.getText(), Integer.parseInt(lengthTrackField.getText()));
								
    							if(tracks.contains(track)){
                                JFrame f= new JFrame();
                                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                JDialog warning = new JDialog(f, "Warning");
                                JLabel labelWarning = new JLabel("Track is already have!", SwingConstants.CENTER);
                                warning.add(labelWarning);
                                warning.setSize(300, 100);
                                warning.setLocation(150, 200);
                                warning.setVisible(true);
    							}else {
                                tracks.add(track);
                                model1.addRow(new Object[]{titleTrackField.getText(), lengthTrackField.getText()});
                                addTrackFrame.dispose();
    							}
    							
    							JFrame askPlayTrack = new JFrame();
    							askPlayTrack.setSize(400, 400);
    							askPlayTrack.setVisible(true);
    							
    							JPanel askTotalJPanel = new JPanel();
    							
    							JPanel askJPanel = new JPanel();
    							
    							JLabel askJLabel = new JLabel();
    							askJLabel.setPreferredSize(new Dimension(300, 25));
    							askJLabel.setText("Do you want to play this track");
    							askJPanel.add(askJLabel);
    							

    							
    							JPanel askButtonJPanel = new JPanel();
    							
    							askButtonJPanel.setPreferredSize(new Dimension(300, 30));
    							JButton yesButton = new JButton("Yes");
    							yesButton.addActionListener(new ActionListener() {
									
									@Override
									public void actionPerformed(ActionEvent e) {
										try {
		        							track.play();		
	        							
		        							JFrame yesPlayFrame = new JFrame();
											yesPlayFrame.setSize(400, 200);
											yesPlayFrame.setVisible(true);
											
											JPanel yesPlayJPanel = new JPanel();
											
											JPanel yesPlayTitleJPanel = new JPanel();
											yesPlayTitleJPanel.setPreferredSize(new Dimension(300, 25));
											
											JLabel yesPlayTitleJLabel = new JLabel();
											yesPlayTitleJLabel.setText("TITLE: ");
											yesPlayTitleJPanel.add(yesPlayTitleJLabel);
											
											JLabel yesPlayTitleJLabel1 = new JLabel();
											yesPlayTitleJLabel1.setText(track.getTitle());
											yesPlayTitleJPanel.add(yesPlayTitleJLabel1);
											
											
											JPanel yesPlayLengthJPanel = new JPanel();
											yesPlayLengthJPanel.setPreferredSize(new Dimension(300, 25));
											JLabel yesPlayLengthJLabel = new JLabel();
											yesPlayLengthJLabel.setText("LENGTH: ");
											yesPlayLengthJPanel.add(yesPlayLengthJLabel);
											
											JLabel yesPlayLengthJLabel1 = new JLabel();

											yesPlayLengthJLabel1.setText(String.valueOf(track.getLength()));
											yesPlayLengthJPanel.add(yesPlayLengthJLabel1);
											
											yesPlayJPanel.add(yesPlayTitleJPanel);
											yesPlayJPanel.add(yesPlayLengthJPanel);
											
											yesPlayFrame.add(yesPlayJPanel);					
											
		        							
		        						}catch(PlayerException error) {
		        							new Warning(error.getWarningMessage());
		        						}	
									}
								});
    							
    							askButtonJPanel.add(yesButton);
    							
    							JButton noButton = new JButton("No");
    							noButton.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										askPlayTrack.dispose();
									}
								});
    							askButtonJPanel.add(noButton);
    							
    							askTotalJPanel.add(askJPanel);
    							askTotalJPanel.add(askButtonJPanel);
    							askPlayTrack.add(askTotalJPanel);
							}
						});
						
						container.add(addTrackButton);
						addTrackFrame.add(container);
					}
				});
				subPane.add(addTrackButton);
				
//				JButton playTrackButton = new JButton("PLAY THIS TRACK");
//				playTrackButton.addActionListener(new ActionListener() {
//					@Override
//					public void actionPerformed(ActionEvent e) {
//						
//						JFrame play = new JFrame();
//						play.setSize(350, 300);
//						play.setTitle("PLAY TRACK");
//						play.setVisible(true);
//						
//						JPanel playJPanel = new JPanel();
//						playJPanel.setSize(400, 200);
//						playJPanel.setPreferredSize(new Dimension(400, 200));
//						JScrollPane scrollPaneTable = new JScrollPane();
//						scrollPaneTable.setPreferredSize(new Dimension(400, 150));
//						scrollPaneTable.setViewportView(table2);
//						
//						DefaultTableModel model2 = new DefaultTableModel();
//						model2.addColumn("TILE");
//						model2.addColumn("LENGTH");
//			
//						for(Track track: tracks) {
//							model2.addRow(new Object[] {track.getTitle(), track.getLength()});
//						}
//						
//						table2.setModel(model2);
//						playJPanel.add(table2);
//						play.add(playJPanel);
//						
//						JPanel totalLengthJPanel = new JPanel();
//						
//						JLabel totalLengthJLabel = new JLabel();
//						totalLengthJLabel.setPreferredSize(new Dimension(200, 25));
//						totalLengthJLabel.setText("TOTAL LENGTH: ");
//						totalLengthJPanel.add(totalLengthJLabel);
//						
//						JTextField totalLengthField = new JTextField();
//						totalLengthField.setPreferredSize(new Dimension(250, 25));
//						int sum = 0;
//						for(Track element:tracks) {
//							sum += element.getLength();
//						}
//						String totalLengthString = String.valueOf(sum);
//						totalLengthField.setText(totalLengthString);
//						totalLengthJPanel.add(totalLengthField);
//						
//						play.add(totalLengthJPanel);
//					}
//				});
//				subPane.add(playTrackButton);
				
				JButton addCDButton = new JButton("ADD CD");
				addCDButton.setPreferredSize(new Dimension(600, 25));
				addCDButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							CompactDisc newCD = new CompactDisc(Integer.parseInt(idField.getText()), titleField.getText(), categoryField.getText(), artistField.getText(), Float.parseFloat(costField.getText()));
							
							for (int i = 0; i < tracks.size(); i++) {
	                            newCD.addTrack(tracks.get(i));
	                        }
							for (int j = tracks.size() - 1; j >=0 ; j--) {
								model1.removeRow(j);
							}
							tracks.clear();
							order.addMedia(newCD);
							idField.setText("");
							titleField.setText("");
							categoryField.setText("");
							costField.setText("");
							artistField.setText("");
						}
						catch(NumberFormatException error) {
							JOptionPane.showMessageDialog(null, "INVALID INPUT TYPE");
						} 
						
						try {
							CompactDisc newCD = new CompactDisc(Integer.parseInt(idField.getText()), titleField.getText(), categoryField.getText(), artistField.getText(), Float.parseFloat(costField.getText()));
							
							for (int i = 0; i < tracks.size(); i++) {
	                            newCD.addTrack(tracks.get(i));
	                        }
							newCD.play();
						} catch(PlayerException error1) {
							new Warning(error1.getWarningMessage());
						}
					}
				});	
				subPane.add(addCDButton);
				contentPane.add(subPane);
				this.revalidate();
				this.repaint();

				
				break;
			case "Book":
				subPane.removeAll();
				showInfo();
				
				// AUTHOR
				JPanel authorJPanel = new JPanel();
				JLabel author = new JLabel();
				author.setText("AUTHOR(separate by ','): ");
				author.setPreferredSize(new Dimension(200, 25));
				authorJPanel.add(author);
				
				JTextField authorField = new JTextField();
				authorField.setPreferredSize(new Dimension(300, 75));
				authorJPanel.add(authorField);
				subPane.add(authorJPanel);
				
				// CONTENT
				JPanel contentJPanel = new JPanel();
				JLabel content = new JLabel();
				content.setText("CONTENT: ");
				content.setPreferredSize(new Dimension(200, 25));
				contentJPanel.add(content);
				
				JTextField contentField = new JTextField();
				contentField.setPreferredSize(new Dimension(300, 50));
				contentJPanel.add(contentField);
				subPane.add(contentJPanel);
				
				JButton addBookButton = new JButton("ADD BOOK");
				addBookButton.setPreferredSize(new Dimension(600, 25));
				addBookButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							List<String> authorStrings = new ArrayList<String>();
							for (int i = 0; i < authorField.getText().split(",").length; i++) {
								authorStrings.add(authorField.getText().split(",")[i]);
							}
							Book newBook = new Book(Integer.parseInt(idField.getText()), titleField.getText(), categoryField.getText(), Float.parseFloat(costField.getText()), authorStrings);
							order.addMedia(newBook);
						} catch(NumberFormatException error) {
							JOptionPane.showMessageDialog(null, "INVALID INPUT TYPE");
							idField.setText("");
							titleField.setText("");
							categoryField.setText("");
							costField.setText("");
							authorField.setText("");
							contentField.setText("");
						}
						
						idField.setText("");
						titleField.setText("");
						categoryField.setText("");
						costField.setText("");
						authorField.setText("");
						contentField.setText("");
					}
				});	
				subPane.add(addBookButton);
				contentPane.add(subPane);
				this.revalidate();
				this.repaint();
				break;
			case "DVD": 
				subPane.removeAll();
				showInfo();
				
				// DIRECTOR
				JPanel directorJPanel = new JPanel();
				JLabel director = new JLabel();
				director.setText("DIRECTOR: ");
				director.setPreferredSize(new Dimension(200, 25));
				directorJPanel.add(director);
				
				JTextField directorField = new JTextField();
				directorField.setPreferredSize(new Dimension(300, 25));
				directorJPanel.add(directorField);
				subPane.add(directorJPanel);
				
				// LENGTH
				JPanel lengthJPanel = new JPanel();
				JLabel length = new JLabel();
				length.setText("LENGTH: ");
				length.setPreferredSize(new Dimension(200, 25));
				lengthJPanel.add(length);
				
				JTextField lengthField = new JTextField();
				lengthField.setPreferredSize(new Dimension(300, 25));
				lengthJPanel.add(lengthField);
				subPane.add(lengthJPanel);
				
				JButton addDVDButton = new JButton("ADD DVD");
				addDVDButton.setPreferredSize(new Dimension(600, 25));
				addDVDButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							DigitalVideoDisc newDVD = new DigitalVideoDisc(Integer.parseInt(idField.getText()),  titleField.getText(), categoryField.getText(), directorField.getText(), Integer.parseInt(lengthField.getText()), Float.parseFloat(costField.getText()));
						} catch(NumberFormatException error) {
							JOptionPane.showMessageDialog(null, "INVALID TYPE INPUT");
							idField.setText("");
							titleField.setText("");
							categoryField.setText("");
							costField.setText("");
							directorField.setText("");
							lengthField.setText("");
						}
						try {
							DigitalVideoDisc newDVD = new DigitalVideoDisc(Integer.parseInt(idField.getText()),  titleField.getText(), categoryField.getText(), directorField.getText(), Integer.parseInt(lengthField.getText()), Float.parseFloat(costField.getText()));
							newDVD.play();
							order.addMedia(newDVD);
						}catch(PlayerException error) {
							new Warning(error.getWarningMessage());
						}
						
						idField.setText("");
						titleField.setText("");
						categoryField.setText("");
						costField.setText("");
						directorField.setText("");
						lengthField.setText("");
						
					}
				});	
				subPane.add(addDVDButton);
				contentPane.add(subPane);
				this.add(contentPane);
				this.revalidate();
				this.repaint();
				break;	
		}
	}
		
	public JMenuBar Menu() {
		JMenuBar jMenuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		contentPane.add(menu);
		JMenuItem createNewOrderItem = new JMenuItem("Create New Order");
		JMenuItem addItemToOrder = new JMenuItem("Add Item To Order");
		JMenuItem deleteItemById = new JMenuItem("Delete Item By Id");
		JMenuItem displayItemList = new JMenuItem("Display Item List Of Order");
		JMenuItem luckyItem = new JMenuItem("Lucky Item");
		JMenuItem exit = new JMenuItem("Exit");
		MenuListener menuListener = new MenuListener();
		createNewOrderItem.addActionListener(menuListener);
		addItemToOrder.addActionListener(menuListener);
		deleteItemById.addActionListener(menuListener);
		displayItemList.addActionListener(menuListener);
		exit.addActionListener(menuListener);
		luckyItem.addActionListener(menuListener);
		menu.add(createNewOrderItem);
		menu.add(addItemToOrder);
		menu.add(deleteItemById);
		menu.add(displayItemList);
		menu.add(luckyItem);
		menu.add(exit);
		jMenuBar.add(menu);
		return jMenuBar;
	}
	
	private class MenuListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			setCommandString(e.getActionCommand());
			showGUIOption(getCommandString());
		}
	}
}
