package hust.soict.globalict.aims;

import java.lang.Exception;

public class PlayerException extends Exception{

	private String warningMessage;
	
	public PlayerException(String string) {
		this.warningMessage = string;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

}
