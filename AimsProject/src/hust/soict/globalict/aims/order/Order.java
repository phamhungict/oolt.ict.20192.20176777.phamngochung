package hust.soict.globalict.aims.order;

import java.util.ArrayList;
import java.util.Random;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order{
	
	public static final int MAX_LIMITTED_ORDERS = 5;
	
	public static final int MAX_NUMBER_ORDERED = 10;
	
	private MyDate dateOrdered = new MyDate();
	
	private ArrayList<Media> itemOrdered = new ArrayList<Media>(MAX_NUMBER_ORDERED);
	
	private static int nbOrder = 0;
	
	// GETTER and SETTER
	
	public ArrayList<Media> getItemOrdered() {
		return itemOrdered;
	}

	public void setItemOrdered(ArrayList<Media> itemOrdered) {
		this.itemOrdered = itemOrdered;
	}
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	// CONSTRUCTORS

	public Order() {
		super();
	}

	public Order(ArrayList<Media> media, String d) {
		super();
		this.itemOrdered = media;
		this.dateOrdered = new MyDate(d);
		nbOrder++;
		if(nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("You have ordered too many times!");
		}
		else {
			System.out.println("You still have "+(MAX_LIMITTED_ORDERS - nbOrder)+" oders to go!");
		}
	}
	
	// ADD MEDIA
//	public void addMedia(Media...medias){
//		if(itemOrdered.size() + medias.length > MAX_NUMBER_ORDERED) {
//			System.out.println("Too much Media");
//		}
//		else {
//			Collections.addAll(itemOrdered, medias);
//		}
//	}
	
	// ADD MEDIA
	public void addMedia(Media media) {
		if(!this.itemOrdered.contains(media)) {
			this.itemOrdered.add(media);
			System.out.println("\nAdd media succesfully\n");
		}
		else {
			System.out.println("\nThe media is already exists !!!\n");
		}
	}
	
	// REMOVE MEDIA
	public void removeMedia(Media media){
		if(!this.itemOrdered.contains(media)) {
			System.out.println("\nThis track doesn't exist in the lists of tracks\n");
		}
		else {
			this.itemOrdered.remove(media);
			System.out.println("\nRemove successfully!!!\n");
		}
	}
	
	// FIND THE ID OF MEDIA
//	private int equals(int id) {
//		for(int i = 0; i < this.itemOrdered.size(); i++) {
//			if(this.itemOrdered.get(i).getId() == id) {
//				return id;
//			}
//		}
//		return -1;
//	}
	
	// REMOVE MEDIA BY ID
	public void removeMediaById(int id) {
		int check = 0;
		Media media = null;
		for(int i = 0; i < itemOrdered.size(); i++) {
			if(itemOrdered.get(i).getId() == id){
				check = 1;
				media = itemOrdered.get(i);
			}
		}
		if(check == 0) {
			System.out.println("The media doesn't exists int the list.\n");
		}
		else {
			System.out.println("The media are removing ... \n");
			itemOrdered.remove(media);
		}
	}
	
	
	// TOTAL COST
	public double totalCost() {
		double total = 0.0;
		for(int i = 0; i < itemOrdered.size(); i++) {
			total += itemOrdered.get(i).getCost();
		}
		total = (double)Math.round(total*100)/100;
		return total;
	}
	
	// PRINT ORDER
	
	public void printOrder() {
		for (Media item: itemOrdered) {
			item.printMedia();
		}
	}
	
	// TOTAL COST HAVE LUCKY ITEMS
	public double totalLuckyCost(int k) {
		double total = 0.0;
		for(int i = 0; i < itemOrdered.size(); i++) {
			if(i == k) {
				total += 0;
			}
			else {
				total += itemOrdered.get(i).getCost();
			}
		}
		total = (double)Math.round(total*100)/100;
		return total;
	}
	
	
//	// PRINT THE ORDER
//	public void printOrder() {
//		System.out.println("\n***************************** ORDER ********************************");
//		System.out.println("\nDate: " + dateOrdered.getMyDate());
//		System.out.println("\nOrder Items: ");
//		for(int i = 0; i < itemOrdered.size(); i++) {
//			System.out.println("\nID: " + itemOrdered.get(i).getId() + " - TYPE: " + itemOrdered.get(i).getMediaForm() + " - TITLE: " + itemOrdered.get(i).getTitle() + " - CATEGORY: " 
//									+ itemOrdered.get(i).getCategory()  + " - COST: " + itemOrdered.get(i).getCost() + "$");
//		}
//		System.out.println("\nTotal Cost: " + this.totalCost() + "$");
//		System.out.println("\n**********************************************************************");
//	}
//	
	// PRINT LUCKY ORDER
	public void printLuckyOrder(int k) {
		System.out.println("\n***************************** LUCKY ORDER ********************************");
		System.out.println("\nDate: " + dateOrdered.getMyDate());
		System.out.println("\nOrder Items: ");
		for(int i = 0; i < itemOrdered.size(); i++) {
			System.out.println("\n" + itemOrdered.get(i).getId() + ". DVD - " + itemOrdered.get(i).getTitle() + " - " + itemOrdered.get(i).getCategory() + " - " + itemOrdered.get(i).getCost() + "$");
		}
		System.out.println("\nTotal Cost: " + this.totalLuckyCost(k) + "$");
		System.out.println("\n**********************************************************************");
	}
	
//	// + " - DIRECTOR: " + itemOrdered.get(i).getDirector() + " - LENGTH: " + itemOrdered.get(i).getLength()
//	
//	public void search() {
//		Scanner nhap = new Scanner(System.in);
//		
//		System.out.println("Input the title you wanna search : ");
//		String title = nhap.nextLine();
//		int check = 0;
//		int i = 1;
//		for( int k = 0; k < itemOrdered.size(); k++) {
//			if(itemOrdered.get(k).getTitle().contains(title)) {
//				System.out.println(i + ". DVD - " + itemOrdered.get(k).getTitle() + " - " + itemOrdered.get(k).getCategory() +  " - " + itemOrdered.get(k).getCost() + "$");
//				check = 1;
//				i++;
//			}
//		}
//		if(check == 0) System.out.println("The title doesn't exists");
//	}
//	
	public int getAluckyItem() {
		Random rd = new Random();
		int lucky = rd.nextInt(itemOrdered.size());
		for(int i = 0; i < itemOrdered.size(); i++) {
			if(lucky == i && itemOrdered.get(i).getCost() < 100 && itemOrdered.size() > 5 && totalCost() > 1000 ) {
//				System.out.println("\nThe lucky item is:");
//				System.out.println("\nTITLE: " + itemOrdered.get(i).getTitle() + " - " + itemOrdered.get(i).getCategory()  + " - " + itemOrdered.get(i).getCost() + "$");
				return lucky;
			}
		}
//		printLuckyOrder(lucky);
		return -1;
	}

}
