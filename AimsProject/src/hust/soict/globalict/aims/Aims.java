package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.GUI.Screen;
import hust.soict.globalict.aims.GUI.Warning;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;

public class Aims {
		
		public static void main(String[] args) throws PlayerException { 
			// thread 
			MemoryDaemon memoryDaemon = new MemoryDaemon();
			Thread thread = new Thread(memoryDaemon);
			thread.setDaemon(true);
			thread.start();
			new Screen();
			
			Scanner input = new Scanner(System.in);
			int choose = 0;
			Order newOrder = null;
			do {
				showMenu();
				System.out.println("Enter you choice: \n");
				choose = input.nextInt();
				input.nextLine();
				
				switch(choose) {
					case 1:
						newOrder = new Order(); 
						newOrder.getDateOrdered().setMyDate1();
						System.out.println("\nCREATED NEW ORDER !!!");
						break;
					case 2:
						System.out.println("\nADDING NEW ITEM\n");
						if (newOrder == null) {
							System.out.println("\nPLEASE CREATE NEW ORDER !!!\n");
						}
						else {
							int type = 0;
							do {
								subMenu();
								type = Integer.parseInt(input.nextLine());
								
								switch(type) {
									case 1: 
										System.out.print("ID: ");
										int idCD = input.nextInt();
										input.nextLine();						
										System.out.print("TITLE: ");
										String titleCD = input.nextLine();
										System.out.print("CATEGORY: ");
										String categoryCD = input.nextLine();
										System.out.print("ARTIST: ");
										String artistCD = input.nextLine();
										System.out.print("COST: ");
										float costCD = input.nextFloat();
										input.nextLine();
										CompactDisc newCD = new CompactDisc(idCD, titleCD, categoryCD, artistCD, costCD);
										System.out.println("ADD TRACK !!!\n");
										boolean inputTrack = true;
										while(inputTrack) {
											System.out.println("\nEnter \'y\' to continue, enter other character to exit\n");
											String nhap = input.nextLine();
											if(!nhap.contentEquals("y")) {
												inputTrack = false;
												System.out.println("EXIT INPUT TRACK !!!\n");
											}
											else {
												System.out.print("Track's title: ");
												String titleTrack = input.nextLine();
												System.out.print("Track's length: ");
												int lengthTrack = Integer.parseInt(input.nextLine());
												Track newTrack = new Track(titleTrack, lengthTrack);
												newCD.addTrack(newTrack);
											}
										}
										System.out.println("\nTracks is created!\n");
										System.out.println("\nDo you want to play this CD? yes/no");
										String enter= input.nextLine();
										if(enter.equals("yes")) {
											try {
											newCD.play();
											System.out.println("\nThe sum of lengths of all tracks: " + newCD.getLength());
											} catch (PlayerException error) {
												new Warning(error.getWarningMessage());
											}
										}
										else {
											System.out.println("No play !!!\n");
										}
							
										newOrder.addMedia(newCD);
										break;
									case 2:
										try {
											System.out.print("ID: ");
											int idBook = input.nextInt();
											input.nextLine();
											System.out.print("TITLE: ");
											String titleBook = input.nextLine();
											System.out.print("CATEGORY: ");
											String categoryBook = input.nextLine();
											System.out.print("AUTHORS (separate by ',' character) : ");
											String authorsBookList = input.nextLine();
											String [] authorsList = authorsBookList.split(",");
											List<String> authorsBook = new ArrayList<String>();
											for(int i = 0; i < authorsList.length; i++) {
												authorsBook.add(authorsList[i]);
											}
											System.out.print("COST: ");
											float costBook = input.nextFloat();
											input.nextLine();
											Book newBook = new Book(idBook, titleBook, categoryBook,costBook, authorsBook);
											newOrder.addMedia(newBook);
										} catch (NumberFormatException e) {
											System.out.println("INVALID INPUT TYPE");
										}
										break;
									case 3:
										System.out.print("ID: ");
										int idDVD = input.nextInt();
										input.nextLine();
										System.out.print("TITLE: ");
										String titleDVD = input.nextLine();
										System.out.print("CATEGORY: ");
										String categoryDVD = input.nextLine();
										System.out.print("DIRECTOR: ");
										String directorDVD = input.nextLine();
										System.out.print("LENGTH: ");
										int lengthDVD = input.nextInt();
										input.nextLine();
										System.out.print("COST: ");
										float costDVD = input.nextFloat();
										input.nextLine();
										DigitalVideoDisc newDVD = new DigitalVideoDisc(idDVD, titleDVD, categoryDVD, directorDVD, lengthDVD, costDVD);
										try {
											newDVD.play();
										}catch(PlayerException e) {
											new Warning(e.getWarningMessage());
										}
										newOrder.addMedia(newDVD);
										break;
									case 0: 
										System.out.println("\nExit!!!\n");
										break;
									default:
										System.out.println("Please choose a number: 0-1-2-3\n");
										break;
								}
							} while (type != 0);
							
						}
						break;
					case 3:
						if (newOrder == null) {
							System.out.println("\nPLEASE CREATE ORDER !!! \n");
						}
						else {
							int id ;
							System.out.println("Enter the id you wanna delete: ");
							id = input.nextInt();
							newOrder.removeMediaById(id);
							System.out.println("\nDELETED\n");
						}
						break;
					case 4: 
						if (newOrder == null) {
							System.out.println("\nPLEASE CREATE ORDER !!! \n");
							break;
						}
						else {
							System.out.println("\nLIST OF ITEMS: ");
							System.out.println("\nDATE: " + newOrder.getDateOrdered().getMyDate1());
							java.util.Collections.sort((java.util.List)newOrder.getItemOrdered());
							for(Media item: newOrder.getItemOrdered()) {
									item.printMedia();
							}
						}
						System.out.println("\nTOTAL COST: " + newOrder.totalCost() + "$" );
						break;
						
					case 0:
						System.out.println("EXIT !!!");
						break;
					default :
						System.out.println("Please choose a number: 0-1-2-3-4\n");
						break;
				}
				
			} while (choose != 0);
			input.close();
		}
		
		public static void showMenu() {
			System.out.println("\n\nOrder Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4\n");
		}
		
		public static void subMenu() {
			System.out.println("\n\nMEDIA TYPE OPTION : ");
			System.out.println("--------------------------------");
			System.out.println("1. CD");
			System.out.println("2. Book");
			System.out.println("3. DVD");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3\n");
		}
}

	

		





