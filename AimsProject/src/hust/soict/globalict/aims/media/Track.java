package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public class Track implements Playable{
	private String title;
	private int length;
	private int nbTracks = 0;
	
	// GETTER
	
	public String getTitle() {
		return title;
	}
	public int getLength() {
		return length;
	}
	
	// CONSTRUCTOR
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
		nbTracks++;
	}
	
	// METHOD
	public void play() throws PlayerException {
		if(this.getLength() > 0) {
			System.out.println("Playing Track: " + this.getTitle());
			System.out.println("Track length: " + this.getLength());
		} else {
			throw new PlayerException("Track length is non-positive");
		}
	}
	
	@Override
	public boolean equals(Object object) {
		if( this.title.equals(((Track) object).getTitle()) && this.length == (((Track) object).getLength())) {
			return true;
		}
		return false;
	}
	

	
	
}
