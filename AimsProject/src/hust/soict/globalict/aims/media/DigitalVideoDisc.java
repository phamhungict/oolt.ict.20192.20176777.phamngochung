package hust.soict.globalict.aims.media;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import hust.soict.globalict.aims.PlayerException; 

public class DigitalVideoDisc extends Disc implements Playable{
	
	// CONSTRUCTOR	
	
	public DigitalVideoDisc() {
		super();
	}
	
	public DigitalVideoDisc(int id, String title, String category, String director, int length, float cost) {
		super(id, title, category, director, length, cost);
	}
	
	// METHOD	
	
	public void play() throws PlayerException {
		if(this.getLength() > 0) {
			System.out.println("Playing DVD: " + this.getTitle());
			System.out.println("DVD length: " + this.getLength());
		} else {
			throw new PlayerException("DVD length is non-positive");
		}
	}
	
	public void printMedia() {
		System.out.println("\n" + this.getId() + ". DVD - " + this.getTitle() + " - " + this.getCategory() 
						+ " - " + this.getDirector() + " - " +  this.getLength() + " - " + this.getCost() + "$");
	}
	
	@Override
	public int compareTo(Object obj) {
		DigitalVideoDisc dvd = (DigitalVideoDisc) obj;
		if(this.getCost() > dvd.getCost()) {
			return 1;
		}
		if (this.getCost() < dvd.getCost()) {
			return -1;
		}
		return 0;
	}
	
	@Override
	public JFrame showDetail() {
		JFrame DVDDetail = new JFrame();
		DVDDetail.setSize(500, 500);
		DVDDetail.setTitle("SHOW DETAIL");
		DVDDetail.setVisible(true);
		
		JPanel DVDDetailJPanel = new JPanel();
		JTable DVDTable = new JTable();
		
		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(10, 11, 414, 329);
		DVDDetailJPanel.add(scrollPaneTable);
		scrollPaneTable.setViewportView(DVDTable);
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("TITLE");
		model.addColumn("CATEGORY");
		model.addColumn("COST");
		model.addColumn("DIRECTOR");
		model.addColumn("LENGTH");

		model.addRow(new Object[] {this.getId(), this.getTitle(), this.getCategory(), this.getCost(), this.getDirector(), this.getLength()});
		
		DVDTable.setModel(model);
		DVDDetail.add(DVDDetailJPanel);
		
		return DVDDetail;
	}
	
}
