package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Book extends Media {
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();
	
	// SETTER AND GETTER

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public List<String> getContentTokens() {
		return contentTokens;
	}
	
	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}
	
	// CONSTRUCTOR
	
	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
	}
	
	public Book(int id, String title, String category, float cost, List<String> authors, String content) {
		super(id, title, category, cost);
		this.authors = authors;
		this.content = content;
	}
	
	
	
	// METHOD
	public void addAuthor(String authorName) {
		Iterator<String> i = authors.iterator();
		while(i.hasNext()) {
			if(i.next().equals(authorName)) {
				System.out.println("The author has already exists in the list!!!\n");
			}
		}
		authors.add(authorName);
		System.out.println("The author is added\n");
	}
	
	public void removeAuthor(String authorName) {
		Iterator<String> i = authors.iterator();
		int check = 0;
		while(i.hasNext()) {
			if(i.next().equals(authorName)) {
				check = 1;
			}
		}
		if(check == 0) {
			System.out.println("The author doesn't exists int the list.\n");
		}
		else {
			System.out.println("The author are removing ... \n");
			authors.remove(authorName);
		}
	}
	
	// Overriding printMedia 
	public void printMedia() {
		System.out.println("\n" + this.getId() + ". BOOK - " + this.getTitle() + " - " + this.getCategory() + " - " + this.getAuthors() + " - " + this.getCost() + "$");
	}
	
	// Process Content Method
	public void processContent(String content) {
		String [] items = content.split("[, ?.]+");
		for(String item: items) {
			contentTokens.add(item);
		}
		
		for (String word: contentTokens) {
			Integer freq = wordFrequency.get(word);
			wordFrequency.put(word, (freq == null) ? 1 : freq + 1);
		}
		
		System.out.println("\nFrequecy of Tokens in the Content: ");
		for(Map.Entry token: wordFrequency.entrySet()) {
			System.out.println("\'" + token.getKey() + "\' : " + token.getValue());
		}
	}
	
	@Override
	public String toString(){
		System.out.println("\nBOOK INFORMATION: ");
		return this.getId() + ". " + this.getTitle() + " - " + this.getCategory()  + " - "
					+ this.getAuthors() + " - " + this.getCost() + "$"  
					+ "\nLENGTH OF CONTENT: " + this.getContentTokens().size()
					+ "\nTHE TOKEN LIST: " + this.getContentTokens()
					+ "\nWORD FREQUENCY: " + this.getWordFrequency();
		
	}
	@Override
	public JFrame showDetail() {
		JFrame bookDetail = new JFrame();
		bookDetail.setSize(500, 500);
		bookDetail.setTitle("SHOW DETAIL");
		bookDetail.setVisible(true);
		
		JPanel bookDetailJPanel = new JPanel();
		JTable bookTable = new JTable();
		
		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(10, 11, 414, 329);
		bookDetailJPanel.add(scrollPaneTable);
		scrollPaneTable.setViewportView(bookTable);
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("TITLE");
		model.addColumn("CATEGORY");
		model.addColumn("COST");
		model.addColumn("AUTHORS");

		model.addRow(new Object[] {this.getId(), this.getTitle(), this.getCategory(), this.getCost(), this.getAuthors()});
		
		bookTable.setModel(model);
		bookDetail.add(bookDetailJPanel);
		
		return bookDetail;
	}

	

	

}
