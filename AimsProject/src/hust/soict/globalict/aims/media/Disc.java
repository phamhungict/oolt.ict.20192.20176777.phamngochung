package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public class Disc extends Media{
	private int length;
	private String director;
	
	// GETTER
	
	public int getLength() {
		return length;
	}
	
	public String getDirector() {
		return director;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	// CONSTRUCTOR
	public Disc() {
		super();
	}
	
	public Disc(int id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
	
	public Disc(int id, String title, String category, String director, int length, float cost) {
		super(id, title, category, cost);
		this.length = length;
		this.director = director;
	}
	
	// METHOD
	
	public void printMedia() {
		System.out.println("\nID: " + this.getId()+ "TITLE: " + this.getTitle() + " - CATEGORY: " + this.getCategory() + " - COST: " + this.getCost() + "$");
	}
	
}
