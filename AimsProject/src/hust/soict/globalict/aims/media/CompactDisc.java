package hust.soict.globalict.aims.media;

import java.util.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import hust.soict.globalict.aims.PlayerException;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	Scanner input = new Scanner(System.in);
	
	// GETTER
	
	public String getArtist() {
		return artist;
	}
	
	public ArrayList<Track> getTracks(){
		return this.tracks;
	}
	
	// CONSTRUCTOR
	
	public CompactDisc(int id, String title, String category,String artist, float cost) {
		super(id, title, category, cost);
		this.artist = artist;
	}
	
	public CompactDisc(int id, String title, String category, String artist, float cost,ArrayList<Track> tracks){
		super(id, title, category, cost);
		this.artist = artist;
		this.tracks = tracks;
	}
	
	// ADD TRACK
	public void addTrack(Track track) {
		if(!this.tracks.contains(track)) {
			this.tracks.add(track);
			System.out.println("\nAdd track successfully\n");
		}
		else {
			System.out.println("\nThis track is already exist in the lists of tracks\n");
		}
		this.setLength(this.getLength());
	}
	
	// REMOVE TRACK
	public void removeTrack(Track track) {
		if(!this.tracks.contains(track)) {
			System.out.println("\nThis track doesn't exist in the lists of tracks\n");
		}
		else {
			this.tracks.remove(track);
			System.out.println("\nRemove successfully!!!\n");
		}
	}
	
//	// FIND TRACK INDEX
//	private int indexTrack(String title, int length) {
//		for (int i = 0; i < this.tracks.size(); i++) {
//			if(this.tracks.get(i).getTitle().equals(title) && this.tracks.get(i).getLength() == length) {
//				return i;
//			}
//		}
//		return -1;
//	}
	
	
	// GET LENGTH
	public int getLength() {
		int sum = 0;
		for(Track track:tracks) {
			sum += track.getLength();
		}
		setLength(sum);
		return sum;
	}
	
	// PLAY METHOD
	public void play() throws PlayerException {
		
		if (this.getLength() > 0) {
			for(Track track:tracks) {
				try {
					track.play();
				}
				catch(PlayerException e) {
					throw new PlayerException("Length of " + this.getTitle() + " is non-positive!");
				}
			}
		}
		else {
			throw new PlayerException("CD length is non-positive");
		}
		
	}
	
	// PRINT CD
	public void printMedia() {
		System.out.println("\n"+ this.getId() + ". CD - " + this.getTitle() + " - " + this.getCategory() + " - " + this.getArtist() + " - " + this.getCost() + "$");
//		this.play();
		System.out.println("\nThe sum of length of all tracks: " + this.getLength() );
	}
	
	@Override
	public int compareTo(Object obj) {
		CompactDisc cd = (CompactDisc) obj;
		if(this.getTracks().size() < cd.getTracks().size()) {
			return -1;
		} 
		if (this.getTracks().size() > cd.getTracks().size()) {
			return 1;
		}
		if(this.getTracks().size() == cd.getTracks().size()){
			if(this.getLength() < cd.getLength()){
				return -1;
			}
			else if ( this.getLength() > cd.getLength()) {
				return 1;
			}
			else {
				return 0;
			}
		}
		return 0;		
	}
	
	@Override
	public JFrame showDetail() {
		JFrame CDDetail = new JFrame();
		CDDetail.setSize(500, 500);
		CDDetail.setTitle("SHOW DETAIL");
		CDDetail.setVisible(true);
		
		JPanel CDDetailJPanel = new JPanel();
		JTable CDTable = new JTable();
		
		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(10, 11, 414, 329);
		CDDetailJPanel.add(scrollPaneTable);
		scrollPaneTable.setViewportView(CDTable);
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("TITLE");
		model.addColumn("CATEGORY");
		model.addColumn("COST");
		model.addColumn("LENGTH");
		model.addColumn("ARTIST");

		model.addRow(new Object[] {this.getId(), this.getTitle(), this.getCategory(), this.getCost(), this.getLength(), this.getArtist()});
		
		CDTable.setModel(model);
		CDDetail.add(CDDetailJPanel);
		
		return CDDetail;
	}
	
}
