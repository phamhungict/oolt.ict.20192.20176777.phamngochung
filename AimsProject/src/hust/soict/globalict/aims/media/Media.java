package hust.soict.globalict.aims.media;

import javax.swing.JFrame;

public abstract class Media implements Comparable {
	private int id;
	private String title;
	private String category;
	private float cost;
		
	// GETTER
	
	public int getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getCategory() {
		return category;
	}
	
	public float getCost() {
		return cost;
	}
	
	// CONSTRUCTOR
	
	public Media() {
		
	}
	
	public Media(int id, String title, String category, float cost) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	@Override
	public boolean equals(Object object) {
		if(this.id == ((Media) object).getId() && this.cost == ((Media) object).getCost()) {
			return true;
		}
		return false;
	}
	
	@Override
	public int compareTo(Object obj) {
		Media media = (Media) obj;
//		return this.getTitle().compareTo(media.getTitle());
		
		if (this.getTitle().compareTo(media.getTitle()) == 0 && this.getCost() == media.getCost()) {
			return 0;
		}
		else {
			return -1;
		}
	}
	
	public void printMedia() {
		System.out.println("\nID: " + this.getId()+ " - TITLE: " + this.getTitle() + " - CATEGORY: " + this.getCategory() + " - COST: " + this.getCost() + "$");
	}
	
	public JFrame showDetail() {
		return new JFrame();
	}
	
}
