package hust.soict.globalict.aims.utils;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate date1 = new MyDate();
		date1.print();
		date1.print1();
		date1.print2();
		date1.accept();
		
		MyDate[] dateArray = {new MyDate(02,10,2019), new MyDate(01,05,2015), new MyDate(8,06,2015), new MyDate(04,05,1983)};
		DateUtils date = new DateUtils();
		
		date.sortTheDate(dateArray);
		date.printDates(dateArray);
	}

}
