package hust.soict.globalict.aims.utils;

public class DateUtils {
	public boolean isLater(MyDate date1, MyDate date2) {
		int day1 = date1.getDay();
		int month1 = date1.getMonth();
		int year1 = date1.getYear();
		
		int day2 = date2.getDay();
		int month2 = date2.getMonth();
		int year2 = date2.getYear();
		
		if(year1 > year2) {
			return true;
		}else if(year1 < year2) {
			return false;
		}else {
			if(month1 > month2) {
				return true;
			}else if(month1 < month2) {
				return false;
			}else {
				if(day1 > day2) {
					return true;
				}else if(day1 < day2) {
					return false;
				}else {
					return false;
				}
			}
		}
	}
	
	public void swap(MyDate date1, MyDate date2) {
		int dayTemp = date1.getDay();
		int monthTemp = date1.getMonth();
		int yearTemp = date1.getYear();
		String timeTemp = date1.getMyDate();
		
		date1.setDay(date2.getDay());
		date1.setMonth(date2.getMonth());
		date1.setYear(date2.getYear());
		date1.setMyDate(date2.getMyDate());
		
		date2.setDay(dayTemp);
		date2.setMonth(monthTemp);
		date2.setYear(yearTemp);
		date2.setMyDate(timeTemp);
	}
	
	public void sortTheDate(MyDate[] dateArr) {
		int n = dateArr.length;
		for(int i=0;i<n;i++) {
			for(int j=i+1;j<n;j++) {
				if(isLater(dateArr[i], dateArr[j])==true) {
					swap(dateArr[i], dateArr[j]);
				}
			}
		}
	}
	
	public void printDates(MyDate[] dateArr) {
		int n = dateArr.length;
		for(int i=0;i<n;i++) {
			System.out.println(dateArr[i].getDay()+"-"+dateArr[i].getMonth()+"-"+dateArr[i].getYear());
		}
	}

}
