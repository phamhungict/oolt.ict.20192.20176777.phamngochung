package hust.soict.globalict.aims.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MyDate {
	
	private int day, month, year;
	private String time;
	
	// CONSTRUCTOR
	
	Calendar now = Calendar.getInstance();
	
	public MyDate(){
		this.day = now.get(Calendar.DATE);
		this.month = now.get(Calendar.MONTH);
		this.year = now.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	// Overloading constructor my date
	
	public MyDate(String day, String month, String year) {
		this.day = xulyngay1(day);
		this.month = xulyThang(month);
		this.year = xulyNam1(year);
	}
	
	
	
	public MyDate(String time) {
		this.time = time;
	}

	public String getMyDate() {
		return time;
	}
	
	public void setMyDate(String time) {
		this.time = time;
	}
	
	public void setMyDate1() {
		this.day = now.get(Calendar.DATE);
		this.month = now.get(Calendar.MONTH)+1;
		this.year = now.get(Calendar.YEAR);
	}
	
	public String getMyDate1(){
		return day + "/" + month + "/" + year;
	}
	
	// SETTER AND GETTER

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public int xulyThang(String s) {
		int thang = 0;
		if(s.equals("january") || s.equals("January") || s.equals("1")) {
			thang = 1;
		}
		if(s.equals("february") || s.equals("February") || s.equals("2")) {
			thang = 2;
		}
		if(s.equals("march") || s.equals("March") || s.equals("3")) {
			thang = 3;
		}
		if(s.equals("april") || s.equals("April") || s.equals("4")) {
			thang = 4;
		}
		if(s.equals("may") || s.equals("May") || s.equals("5")) {
			thang = 5;
		}
		if(s.equals("june") || s.equals("June") || s.equals("6")) {
			thang = 6;
		}
		if(s.equals("july") || s.equals("July") || s.equals("7")) {
			thang = 7;
		}
		if(s.equals("august") || s.equals("August") || s.equals("8")) {
			thang = 8;
		}
		if(s.equals("september") || s.equals("September") || s.equals("9")) {
			thang = 9;
		}
		if(s.equals("october") || s.equals("October") || s.equals("10")) {
			thang = 10;
		}
		if(s.equals("november") || s.equals("November") || s.equals("11")) {
			thang = 11;
		}
		if(s.equals("december") || s.equals("December") || s.equals("12")) {
			thang = 12;
		}
		return thang;
	}
	
	private int xulyngay1(String str) {
		int ngay = 0;
		String arr[] = {"first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth","eleventh","twelfth","thirteenth","fourteenth","fifteenth","sixteenth","seventeenth","eighteenth","nineteenth","twentyth","twenty-first","twenty-second","twenty-third","twenty-fourth","twenty-fifth","twenty-sixth","twenty-seventh","twenty-eighth","twenty-ninth","thirtieth","thirty-first"};
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].equals(str)) {
				ngay = i+1;
			}
		}
		return ngay;
	}
	
	public int xulyNgay(String s) {
		int ngay = 0;
		String h = "";
		char a[] = s.toCharArray();
		for(int i = 0; i < a.length; i++) {
			if(Character.isDigit((a[i]))) {
				h += a[i];
			}
		}
		ngay = Integer.parseInt(h);
		return ngay;			
	}
	
	public void accept() {
		Scanner input = new Scanner(System.in);
		System.out.println("\nEnter the date: ");
		String date = input.nextLine();
		String parts[] = date.split(" ");
		
		int m = xulyThang(parts[0]);
		int d = xulyNgay(parts[1]);
		int y = Integer.parseInt(parts[2]);
		int check = 1;
		if( m == 0) {
			System.out.println("The date is invalid !!!");
			check = 0;
		}
		else {
			this.month = xulyThang(parts[0]);
			if(m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
				if(d <1 || d > 31) {
					System.out.println("The day is invalid !!!");
					check = 0;
				}
				else {
					this.day = d;
				}
			}
			else if(m == 2) {
				if ((y % 400 == 0) || ((y % 4 == 0) && (y % 100 != 0))) {
	                if(d > 0 &&d <= 29) this.day = d;
	                else System.out.println("The date is invalid !!!");
	                check = 0;
	            } else {
	            	if(d > 0 &&d <= 28) this.day = d;
	            	else System.out.println("The date is invalid !!!");
	            	check = 0;
	            }
			}
			else {
				if(d <1 || d > 30) {
					System.out.println("The date is invalid !!!");
					check = 0;
				}
				else {
					this.day = d;
				}
			}
		}
		if (check == 1) System.out.println("The date is " + d +"/" + m + "/" + y);
	}
	
	public void print() {
		System.out.print("Current date is: ");
		System.out.println(now.get(Calendar.DATE)+"/"+(now.get(Calendar.MONTH)+1)+"/"+now.get(Calendar.YEAR));
	}
	
//	public void printDate(String h) {
//		System.out.println(h);
//	}
	
	public String chuyenThang(int month) {
		String thang = "";
		if(month == 1) thang = "January";
		if(month == 2) thang = "February";
		if(month == 3) thang = "March";
		if(month == 4) thang = "April";
		if(month == 5) thang = "May";
		if(month == 6) thang = "June";
		if(month == 7) thang = "July";
		if(month == 8) thang = "August";
		if(month == 9) thang = "September";
		if(month == 10) thang = "October";
		if(month == 11) thang = "November";
		if(month == 12) thang = "December";
		return thang;
	}
	
	public String chuyenThang1(int month) {
		String thang = "";
		if(month == 1) thang = "Jan";
		if(month == 2) thang = "Feb";
		if(month == 3) thang = "Mar";
		if(month == 4) thang = "Apr";
		if(month == 5) thang = "May";
		if(month == 6) thang = "Jun";
		if(month == 7) thang = "Jul";
		if(month == 8) thang = "Augt";
		if(month == 9) thang = "Sep";
		if(month == 10) thang = "Oct";
		if(month == 11) thang = "Nov";
		if(month == 12) thang = "Dec";
		return thang;
	}
	
	// overloading method print
	public void print1() {
		String n = "";
		int ngay = now.get(Calendar.DATE);
		if(ngay == 1) n= "st";
		if(ngay == 2) n= "nd";
		if(ngay == 3) n = "rd";
		if(ngay > 3) n= "th";
		System.out.println("Current date is : " + chuyenThang((now.get(Calendar.MONTH)+1)) + " " + ngay + n + " " + now.get(Calendar.YEAR));
	}
	
	
	public void menu() {
		System.out.println("\nFORMAT\n");
		System.out.println("1. yyyy-MM-dd\n");
		System.out.println("2. d/M/yyyy\n");
		System.out.println("3. dd-MMM-yyyy\n");
		System.out.println("4. MMM d yyyy\n");
		System.out.println("5. mm-dd-yyyy\n");
	}
	
	
	public void print2() {
		menu();
		Scanner input = new Scanner(System.in);
		System.out.println("Enter option: ");
		int choose = input.nextInt();
		int day = now.get(Calendar.DATE);
		int month = now.get(Calendar.MONTH)+1;
		int year = now.get(Calendar.YEAR);
		System.out.println("\nThe current date is :");
		switch(choose) {
			case 1:
				if (day < 10) {
					System.out.println(year+"-"+month+"-0"+day);
				}
				if(month < 10 ) {
					System.out.println(year+"-0"+month+"-"+day);
				}
				if(day < 10 && month < 10) {
					System.out.println(year+"-0"+month+"-0"+day);
				}
				if( day >= 10 && month >= 10) {
					System.out.println(year+"-"+month+"-"+day);
				}
				
				break;
			case 2: 
				System.out.println(day + "/" + month + "/" + year);
				break;
			case 3: 
				if(day < 10 ) {
					System.out.println("0" + day + "-" + chuyenThang1(month) + "-" + year);
				} else {
					System.out.println(day + "-" + chuyenThang1(month) + "-" + year);
				}
				break;
			case 4: 
				System.out.println(chuyenThang1(month)+ " " + day + " " + year);
				break;
			case 5: 
				if(day < 10 ) {
					System.out.println("0"+day + "-" + month + "-" + year);
				}
				if(month < 10) {
					System.out.println(day + "-0" + month + "-" + year);
				}
				if(day < 10 && month < 10) {
					System.out.println("0"+day + "-0" + month + "-" + year);
				}
				if( day >= 10 && month >= 10) { 
					System.out.println(day + "-" + month + "-" + year);
				}	
				break;
		}
	}
	
	public static final String yearStr[] = {"hundred","one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty","twenty-one","twenty-two","twenty-three",
			"twenty-four","twenty-five","twenty-six","twenty-seven","twenty-eight","twenty-nine","thirty","thirty-one","thirty-two","thirty-three","thirty-four","thirty-five","thirty-six","thirty-seven","thirty-eight","thirty-nine",
			"forty","forty-one","forty-two","forty-three","forty-four","forty-five","forty-six","forty-seven","forty-eight","forty-nine","fifty","fifty-one","fifty-two","fifty-three","fifty-four","fifty-five","fifty-six",
			"fifty-seven","fifty-eight","fifty-nine","sixty","sixty-one","sixty-two","sixty-three","sixty-four","sixty-five","sixty-six","sixty-seven","sixty-eight","sixty-nine","seventy","seventy-one","seventy-two","seventy-three","seventy-four","seventy-five","seventy-six","seventy-seven","seventy-eight","seventy-nine",
			"eighty","eighty-one","eighty-two","eighty-three","eighty-four","eighty-five","eighty-six","eighty-seven","eighty-eight","eighty-nine","ninety","ninety-one","ninety-two","ninety-three","ninety-four",
			"ninety-five","ninety-six","ninety-seven","ninety-eight","ninety-nine"};
	
	public int convert(String str) {
		if(str != null) {
			for(int i=0;i<=99;i++) {
				if(str.compareTo(yearStr[i])==0) {
					return i;
				}
			}
		}
		System.out.println("Invalid");
		return 0;
	}

	public int xulyNam1(String str) {
		int a=0,b=0,c=0;
		int kq = 0;
		String[] strTmp = str.split(" ");
		a = convert(strTmp[0]);
		
		if(strTmp.length>=2 && strTmp[1].compareTo("oh")==0) {
			b = convert(strTmp[2]);
		}else if(strTmp.length>=2 &&strTmp[1].compareTo("thousand")==0) {
			a*=10;
			if(strTmp.length>=3 && strTmp[2].compareTo("and")==0) {
				b = convert(strTmp[3]);
			}else {//five
				b = convert(strTmp[2]);
			}
			
			if(strTmp.length>=4 && strTmp[3].compareTo("hundred")==0) {
				b*=100;
				if(strTmp.length>=5 && strTmp[4].compareTo("and")==0) {
					c = convert(strTmp[5]);
				}
			}
		}else {
			b = convert(strTmp[1]);
			if(a<10 && b<10) {
				a *= 10;
			}
			
			if(b<10) {
				kq = a+b;
			}
		}
		return kq;
	}
	
}

