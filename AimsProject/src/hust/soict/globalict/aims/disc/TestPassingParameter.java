package hust.soict.globalict.aims.disc;

import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestPassingParameter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
//		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
		
//		swap1(jungleDVD, cinderellaDVD);
//		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
//		System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());
//		
//		swap(jungleDVD, cinderellaDVD);
//		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
//		System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());
//	
//		changeTitle(jungleDVD, cinderellaDVD.getTitle());
//		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
//	
//		
//	}
	
//	public static void swap(Object o1, Object o2) {
//		Object tmp = o1;
//		o1 = o2;
//		o2 = tmp;
//	}
	
//	public static void changeTitle(DigitalVideoDisc dvd, String title) {
//		String oldTitle = dvd.getTitle();
//		dvd.setTitle(title);
//		dvd = new DigitalVideoDisc(oldTitle);
	}
	
//	public static void swap1(DigitalVideoDisc o1,  DigitalVideoDisc o2) {
//		// swap title
//		String tempTitle = o1.getTitle();
//		o1.setTitle(o2.getTitle());
//		o2.setTitle(tempTitle);
//		
//		// swap director
//		String tempDirector = o1.getDirector();
//		o1.setDirector(o2.getDirector());
//		o2.setDirector(tempDirector);
//		
//		// swap category
//		String tempCategory = o1.getCategory();
//		o1.setCategory(o2.getCategory());
//		o2.setCategory(o1.getCategory());
//		
//		// swap length
//		int tempLength = o1.getLength();
//		o1.setLength(o2.getLength());
//		o2.setLength(tempLength);
//		
//		// swap cost
//		float tempCost = o1.getCost();
//		o1.setCost(o2.getCost());
//		o2.setCost(tempCost);
//	}

}